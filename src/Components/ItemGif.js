import React from 'react';
import { Card } from 'antd';

const { Meta } = Card;
function ItemGif(props) {
    return (
        <Card
            hoverable
            cover={<img
                alt=''
                src={
                    props.item.images.fixed_width.url
                }
                style={{
                    height: '240px'
                }}
                onClick={() => {
                    props.onClickItem(props.item)
                }}
            />}
        >
            <Meta
                title={props.item.title}
            />
        </Card>
    )
}

export default ItemGif;