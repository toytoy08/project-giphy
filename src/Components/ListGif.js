import React, { Component } from 'react';
import ItemGif from './ItemGif';
import { List,Input } from 'antd';

const { Search } = Input

class ListGif extends Component {
  render() {
    return (
      <div style={{ minHeight: '300px' }}>
         {/* <Search
            placeholder="Search GIFs"
            onSearch={this.props.onSearch} /> */}
        <List
          pagination={{ pageSize: 40, alignItems: 'center' }}
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item>
              <ItemGif
              item={item}
              onClickItem={this.props.onClickItem}
              />
            </List.Item>
          )}
        />
      </div>
    )
  }
}


export default ListGif;
