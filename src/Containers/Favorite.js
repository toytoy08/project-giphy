import React, { Component } from 'react';
import { List, Layout, Empty } from 'antd';
import ItemGif from '../Components/ItemGif';

class FavoritemPage extends Component {
  state = {
    items: []
  }

  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data');
    const email = jsonStr && JSON.parse(jsonStr).email;
    const jsonFavStr = localStorage.getItem(`list-fav-${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      this.setState({ items: items })
    }
  }

  render() {
    // console.log('item :',this.state.items);

    return (  
      // <div>Fav</div>
      <div
        style={{
          padding: '16px',
          minHeight: '300px',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',

        }}
      >
        <Layout
          style={{
            padding: '16px',
            minHeight: '300px',
            minWidth: 'auto',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',

          }}>
          {this.state.items.length > 0 ? (
            <List
              grid={{ gutter: 20, column: 4 }}
              dataSource={this.state.items}
              renderItem={item => (
                <List.Item>
                  <ItemGif
                    item={item}
                    onClickItem={this.props.onClickItem} />
                </List.Item>
              )}
            />
          ) : (
              <Empty/>
            )
          }
        </Layout>
      </div>
    )
  }
}

export default FavoritemPage