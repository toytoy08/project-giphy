import React, { Component } from 'react';
import { Icon, Form, Button, Input, Layout, message } from 'antd';
import { auth, provider } from '../firebase'

const { Content } = Layout

class Login extends Component {
  state = {
    isLoading: false,
    email: '',
    password: '',
    isShowModal: false,
    isLogin: false,
    imageUrl: ''
  }
  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data');
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn
    if (isLoggedIn) {
      this.navigateToMainPage()
    }
  }

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push('/home');
  };

  saveInformationUser = email => {
    localStorage.setItem(
      'user-data',
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    );
    this.setState({ isLoading: false });
    this.navigateToMainPage();
  };

  facebookLogin = () => {
    console.log(this);
    auth.signInWithPopup(provider).then(({ user }) => {
      console.log(this);
      this.setState({ imageUrl: `${user.photoURL}?height=500` });
      this.saveInformationUser(user.email);
    })
  }

  render() {
    return (
      <div
        style={{
          padding: '16px',
          minHeight: '30px',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
        }}
      >
        <div>
          <Layout
            style={{
              padding: '16px',
              minHeight: '30px',
              justifyContent: 'center',
              alignItems: 'center',
              display: 'flex'
            }}>
            <Content>
              <Form onSubmit={this.onSubmitFormLogin}>
                <Form.Item>
                  <Input
                    prefix={<Icon type="user" />}
                    placeholder="Email"
                    onChange={this.onEmailChange}
                  />
                </Form.Item>

                <Form.Item>
                  <Input
                    prefix={<Icon type="lock" />}
                    type="password"
                    placeholder="Password"
                    onChange={this.onPasswordChange}
                  />
                </Form.Item>

                <Form.Item>
                  <Button
                    style={{ width: '44%' }}
                    htmlType="submit"
                    type="primary"
                    onClick={message.error('Login with Email Not finish', 1)}
                  >
                    Login
            </Button>
                </Form.Item>
              </Form>
              <Button
                type="primary"
                icon="facebook"
                onClick={this.facebookLogin}
              >
                Login With Facebook
        </Button>
            </Content>

          </Layout>

        </div>
      </div>
    )
  }
}

export default Login