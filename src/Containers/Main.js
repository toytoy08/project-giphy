import React, { Component } from 'react';
import { Layout, Menu, Icon, Spin, Modal, Button, message } from 'antd';
import RouteMenu from './RouteMenu';
import { CopyClip } from 'react-copy-to-clipboard'

const { Header, Content, Footer } = Layout;
const menus = ['home', 'favorite', 'profile', 'loginAll'];

const apikey = 'CA4RzlRR6V647IspmmsWGmuYEuL0LW9C'
const apiLimit = '400'
const apirate = 'G'
const word = ''
const apiHead = `https://api.giphy.com/v1/gifs/`
const apiGifphyTrading = `https://api.giphy.com/v1/gifs/trending?api_key=${apikey}`
const apiGifphySearch = `https://api.giphy.com/v1/gifs/search?api_key=${apikey}&q=`

class Main extends Component {

    state = {
        items: [],
        pathName: menus[0],
        favItems: [],
        imageUrl: '',
        isLoggedIn: false,
        itemSelected: null,
        isShowItem: false
    };

    onSelectPage = (page) => {
        const pageLoad = (page - 1) * 40
        this.setState({ page: pageLoad }, () => {
            this.onSearchChange(this.state.word)
        });
    };

    loadData = (value) => {
        this.setState({ word: value })
        console.log(value)
        this.setState({ isLoading: true });
        fetch(
            `https://api.giphy.com/v1/gifs/search?api_key=${apikey}&q=${word}&limit=${this.state.page * 40}&offset=${(this.state.page - 1) * 40}&rating=${apirate}`
        )
            .then(response => response.json())
            .then(items => this.setState({ items, isLoading: false }));
    };

    onMenuClick = e => {
        if (e.key) {
            var path = '/';
            path = `/${e.key}`;
            this.props.history.replace(path);
        }
    };

    componentDidMount() {
        const { pathname } = this.props.location;
        var pathName = menus[0];
        if (pathname !== '/') {
            pathName = pathname.replace('/', '');
            if (!menus.includes(pathName)) pathName = menus[0];
        }
        this.setState({ pathName });
        this.fetchdata()
    }

    fetchdata = () => {
        fetch('https://api.giphy.com/v1/gifs/trending?api_key=' + apikey + '&limit=' + apiLimit + '&rating=' + apirate)
            .then(response => response.json())
            .then(items => this.setState({ items: items.data }))
            .catch(err => console.log(err))
    }

    onClickItem = item => {
        this.setState({ itemSelected: item })
        this.setState({ isShowItem: true })
    }

    onModalCancel = () => {
        this.setState({ itemSelected: null })
        this.setState({ isShowItem: false })
    }

    onSearchChange = (Input) => {
        console.log('Input:', Input)
        if (Input === '') {
            fetch(apiHead + apiGifphyTrading + `&limit=40&offset=${this.state.page}&rating=R&lang=en`)
                .then(response => response.json())
                .then(items => this.setState({ items: items.data }))
                .catch(err => console.log(err))
            this.setState({ searchKey: '' })
        } else {
            fetch(apiHead + apiGifphySearch + Input + `&limit=40&offset=${this.state.page}&rating=R&lang=en`)
                .then(response => response.json())
                .then(items => this.setState({ items: items.data }))
                .catch(err => console.log(err))
            this.setState({ searchKey: Input })
        }
    }

    onClickFavoriteItem = () => {
        // const items = this.state.favItems
        // const itemFav = this.state.currentSelected
        // const index = items.findIndex(item => {
        //     return item.slug === itemFav.slug
        // })
        // if (index !== -1) {
        //     items.splice(index, 1)
        //     localStorage.setItem(
        //         `list-fav-${this.state.email}`,
        //         JSON.stringify(items)
        //     )
        //     message.success('You don\'t like this', 1, () => {
        //         this.setState({ favItems: items })
        //         this.onModalCancel()
        //         window.location.reload();
        //     })
        // } else {
        //     items.push(itemFav)
        //     localStorage.setItem(
        //         `list-fav-${this.state.email}`,
        //         JSON.stringify(items)
        //     )
            message.success('Favorited', 1)
            // , () => {
                // this.setState({ favItems: items })
                // this.onModalCancel()
            // })
        // }
    }

    // checkItemFavorited() {
    //     const items = this.state.favItems
    //     const itemGif = this.state.itemSelected
    //     const result = items.find(item => {
    //         return item.slug === itemGif.slug
    //     })
    //     console.log('result', result)

    //     if (result) {
    //         return 'primary'
    //     } else {
    //         return ''
    //     }
    // }

    render() {
        return (
            <div>
                <Layout>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={[menus[0]]}
                            style={{ lineHeight: '64px' }}
                            onClick={e => {
                                this.onMenuClick(e);
                            }}
                        >
                            <Menu.Item key={menus[0]}><Icon type="home" />Home</Menu.Item>
                            <Menu.Item key={menus[1]}><Icon type="star" />Favorite</Menu.Item>
                            <Menu.Item key={menus[2]}><Icon type="user" />Profile</Menu.Item>
                            {this.state.isLoggedIn === false ? (
                                <Menu.Item key={menus[3]}><Icon type="lock" />Login</Menu.Item>
                            ) : (
                                    <div></div>
                                )}
                        </Menu>
                    </Header>
                    <h1>GIPHY</h1>
                    <Content style={{ padding: '0 10%' }}>
                        <Layout style={{ padding: '2% 0', background: '#fff' }}>
                            <Content style={{ padding: '0 2%', minHeight: 280 }}>
                                <div
                                    style={{
                                        padding: '24px 0',
                                        minHright: '0px',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        display: 'flex',
                                        background: '#d5d5d5'
                                    }}>
                                    {this.state.items.length > 0 ? (
                                        <RouteMenu
                                            items={this.state.items}
                                            onClickItem={this.onClickItem}
                                            onSearch={this.onSearch}
                                        />
                                    ) : (
                                            <Spin size="large" />
                                        )}
                                </div>
                            </Content>
                        </Layout>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        Project Giphy ©2019 Powered by Giphy
                </Footer>
                </Layout >

                {this.state.itemSelected != null ? (
                    <div>
                        <Modal
                            width="40%"
                            style={{ maxHeight: '70%' }}
                            title={this.state.itemSelected.title}
                            visible={this.state.isShowItem}
                            onCancel={this.onModalCancel}
                            footer={[
                                <Button
                                    key="fav"
                                    // type={this.checkItemFavorited()}
                                    icon="like"
                                    size="large"
                                    shape="round"
                                    onClick={this.onClickFavoriteItem}
                                ></Button>,
                                // <CopyClip text={this.state.itemSelected.images.original.url}>
                                //     <Button
                                //         key="clipboard"
                                //         type="primary"
                                //         icon="link"
                                //         size="large"
                                //         shape="round"
                                //         onClick={this.onClickCopy}
                                //     >Copy URL</Button>
                                // </CopyClip>
                            ]}
                        >
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginBottom: '16px'
                                }}
                            >
                                <img
                                    src={this.state.itemSelected.images.original.url}
                                    style={{ height: 'auto', width: 'auto' }}
                                    alt=''
                                />
                            </div>
                        </Modal>
                    </div>
                ) : (
                        <div />
                    )}
            </div>
        )
    }
}

export default Main