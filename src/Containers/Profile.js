import React, { Component } from 'react'
import { Layout, Avatar, Icon, Button, message } from 'antd';
import { auth } from '../firebase';

class Profile extends Component {

    state = {
        email: '',
        isLoading: false,
        imageUrl: ''
    }

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data')
        const email = jsonStr && JSON.parse(jsonStr).email
        var imageUrl = jsonStr && JSON.parse(jsonStr).imageUrl
        if (!imageUrl) {
            imageUrl = 'https://icons-for-free.com/free-icons/png/512/1902268.png'
        }
        this.setState({ email, imageUrl })
    }

    onClickLogout = () => {
        this.setState({ isLoading: true })
        localStorage.setItem(
            'user-data',
            JSON.stringify({
                isLoggedIn: false
            })
        )
        setTimeout(() => {
            this.setState({ isLoading: false })
            this.props.history.push('/')
        }, 2000)
    }

    navigateToMainPage = () => {
        const { history } = this.props;
        history.push('/home');
    };

    render() {
        return (
            <div
                style={{
                    padding: '16px',
                    minHeight: '300px',
                    justifyContent: 'center',
                    alignItems: 'center',
                    display: 'flex',
                }}
            >
                <div>
                    <Layout
                        style={{
                            padding: '16px',
                            minHeight: '300px',
                            justifyContent: 'center',
                            alignItems: 'center',
                            display: 'flex'
                        }}>
                        <Avatar shape="square" size={250} src={this.state.imageUrl} />
                        <br />
                        <Button
                            type="primary"
                            block
                            loading={this.state.isLoading}
                            onClick={this.onClickLogout}
                        >
                            Logout
                        </Button>
                    </Layout>
                </div>
            </div>
        )
    }
}

export default Profile