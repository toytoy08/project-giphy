import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListGif from '../Components/ListGif';
import Profile from './Profile'
import Favorite from './Favorite'
import Login from './Login'
import { message } from 'antd';

function RouteMenu(props) {
  return (
    <Switch>
      <Route
        path="/home" exact render={() => {
          return <ListGif
            items={props.items}
            onClickItem={props.onClickItem}
            onSearch={props.onSearch}
          />
        }}
      />
      {/* <Route path="/favorite" exact render={() => {
        return <Favorite
          onClickItem={props.onClickItem}
        />
      }} /> */}
      <Route path="/favorite" exact render={() => {
        return  message.error('Not finish',1)}}
        />
      <Route path="/profile" exact component={Profile} />
      <Route path="/loginAll" exact component={Login} />
      <Redirect from="/*" exact to="/" />
    </Switch>
  );
}

export default RouteMenu;