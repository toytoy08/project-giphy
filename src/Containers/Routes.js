import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MainPage from './Main'

function Routes() {
  return (
    <div>
      <Switch>
        <Redirect from="/" exact to="/home" />
        <Route component={MainPage} />
      </Switch>
    </div>
  );
}

export default Routes;
