import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCLdf-hTViuGppG0gbfdcaYtOZip00WfH8",
    authDomain: "my-react-4bebc.firebaseapp.com",
    databaseURL: "https://my-react-4bebc.firebaseio.com",
    projectId: "my-react-4bebc",
    storageBucket: "my-react-4bebc.appspot.com",
    messagingSenderId: "688741824464"
};

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider }; 